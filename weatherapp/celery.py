import os
from celery import Celery

# set the default Django settings module for the 'celery' program.
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'weatherapp.settings')

app = Celery('weatherapp')

# Using a string here means the worker don't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@app.task
def debug_task(self):
    print('Request: {0!r}'.format(self.request))


app.conf.timezone = 'UTC'

app.conf.beat_schedule = {
    # Executes every day.
    'update-city-weather-15-minutes': {
        'task': 'weatherapp.city_weather.tasks.update_city_weather',
        'schedule': crontab(minute=5, hour=0),
        'args': ()
    },
}
