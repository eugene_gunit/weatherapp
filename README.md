# WeatherApp

Get the weather for your city. (API)

## Prerequisites
- Python 3.6+
- Django 3.0.2+
- PostgreSQL

## Installation
- Clone the project
    ```
    git clone git@bitbucket.org:eugene_gunit/weatherapp.git
    ```
- Create virtual environment
- Install required packages
    ```
    (venv) pip install -r requirements.txt
    ```

- Create a new database (PostgreSQL).

- Apply migrations:
    ```
    python manage.py migrate
    ```

- Create admin user [optional]:
    ```
    python manage.py createsuperuser
    ```

- Add environment variables:
    ```
    Create .env file in the root of project:
    export DATABASE_URL=postgres://<db_user>:<db_pass>@localhost:5432/<db_name>
    export OPENWEATHERMAP_API_KEY=<your_apikey>
    export CELERY_BROKER_URL=redis://127.0.0.1:6379/0
    export CELERY_RESULT_BACKEND=redis://127.0.0.1:6379
    ```

## Running

### `django`

    $ python manage.py runserver

   visit http://127.0.0.1:8000/api/currentWeather/

### `celery`

    $ celery -A weatherapp worker -l info


