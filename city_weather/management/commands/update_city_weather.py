from django.core.management.base import BaseCommand

from city_weather.tasks import update_city_weather


class Command(BaseCommand):
    args = '(none)'
    help = 'Update city weather '

    def handle(self, *args, **options):
        self.stdout.write('Updating...\n')
        update_city_weather()
        self.stdout.write('Done.\n')
