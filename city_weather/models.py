from django.db import models
from django.conf import settings

from city_weather.providers.base import WeatherFactory


class CityWeather(models.Model):
    city_name = models.CharField(max_length=100, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True)
    condition = models.CharField(max_length=100, null=True, blank=True)
    temperature = models.FloatField(blank=True, null=True)
    temperature_min = models.FloatField(blank=True, null=True)
    temperature_max = models.FloatField(blank=True, null=True)
    wind_speed = models.FloatField(blank=True, null=True)

    def __str__(self):
        return 'Weather for city {} [last updated: {}.'.format(
            self.city_name,
            self.updated_at
        )

    def update_weather(self, provider=None):
        if not provider:
            provider = settings.DEFAULT_WEATHER_PROVIDER
        weather_fetcher = WeatherFactory.create_provider(provider=provider)
        self.condition = weather_fetcher.get_condition()
        self.temperature = weather_fetcher.get_temperature()
        self.temperature_min = weather_fetcher.get_temperature_min()
        self.temperature_max = weather_fetcher.get_temperature_max()
        self.wind_speed = weather_fetcher.get_wind_speed()
        self.save()
