from rest_framework import serializers
from .models import CityWeather


class CityWeatherSerializer(serializers.ModelSerializer):
    currentTemp = serializers.FloatField(source='temperature')
    tempMin = serializers.FloatField(source='temperature_min')
    tempMax = serializers.FloatField(source='temperature_max')
    windSpeed = serializers.FloatField(source='wind_speed')
    weatherCondition = serializers.CharField(source='condition')

    class Meta:
        model = CityWeather
        fields = ('currentTemp', 'tempMin', 'tempMax', 'windSpeed',
                  'weatherCondition', )
