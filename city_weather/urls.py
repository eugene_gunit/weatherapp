from django.urls import path

from .views import CityWeatherView, SyncWeatherView

urlpatterns = [
    path('currentWeather/', CityWeatherView.as_view(), name='current-weather'),
    path('syncWeather/', SyncWeatherView.as_view(), name='sync-weather')
]
