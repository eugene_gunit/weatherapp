from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import GenericAPIView, ListAPIView

from .models import CityWeather
from .serializers import CityWeatherSerializer
from .tasks import update_city_weather


class CityWeatherView(ListAPIView):
    serializer_class = CityWeatherSerializer

    def get_queryset(self):
        queryset = CityWeather.objects.all()
        return queryset


class SyncWeatherView(GenericAPIView):
    def get(self, request, *args, **kwargs):
        update_city_weather.apply_async()
        return Response({}, status.HTTP_200_OK)

