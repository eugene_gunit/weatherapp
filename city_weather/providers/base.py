from django.conf import settings
from django.utils.module_loading import import_string
from six import string_types


class WeatherFactory(object):

    @classmethod
    def create_provider(cls, city=None, provider=None):
        if not city:
            city = settings.DUMMY_CITY_DATA
        if not provider:
            provider = settings.DEFAULT_WEATHER_PROVIDER
        if isinstance(provider, string_types):
            provider_instance = import_string(provider)(city)
        else:
            provider_instance = provider(city)
        return provider_instance


class WeatherBase(object):
    def __init__(self, city, *args, **kwargs):
        self.city = city
        self.location = '{}, {}'.format(
            self.city['name'],
            self.city['country_code']
        )

    def get_condition(self):
        raise NotImplementedError

    def get_temperature(self):
        raise NotImplementedError

    def get_temperature_min(self):
        raise NotImplementedError

    def get_temperature_max(self):
        raise NotImplementedError

    def get_wind_speed(self):
        raise NotImplementedError

    def __str__(self):
        return 'Weather info for {0}'.format(self.location)
