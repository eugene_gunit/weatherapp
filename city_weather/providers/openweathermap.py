import pyowm

from django.conf import settings
from .base import WeatherBase


class OpenWeatherMapProvider(WeatherBase):
    def __init__(self, city, *args, **kwargs):
        super(OpenWeatherMapProvider, self).__init__(city)
        owm = pyowm.OWM(settings.OPENWEATHERMAP_API_KEY)
        obs = owm.weather_at_place(self.location)
        self.today_forecast = obs.get_weather()

    def get_condition(self):
        return self.today_forecast.get_status()

    def get_temperature(self):
        return self.today_forecast.get_temperature(unit='celsius')['temp']

    def get_temperature_min(self):
        return self.today_forecast.get_temperature(unit='celsius')['temp_min']

    def get_temperature_max(self):
        return self.today_forecast.get_temperature(unit='celsius')['temp_max']

    def get_wind_speed(self):
        return self.today_forecast.get_wind()['speed']
