import logging

from django.conf import settings


from .models import CityWeather
from celery import shared_task

logger = logging.getLogger(__name__)


@shared_task
def update_city_weather():
    logger.debug('Update city weather task is running')
    city_name = settings.DUMMY_CITY_DATA.get('name')
    city_weather, _ = CityWeather.objects.get_or_create(city_name=city_name)
    if city_weather:
        city_weather.update_weather()
