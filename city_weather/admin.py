from django.contrib import admin

from .models import CityWeather


class CityWeatherAdmin(admin.ModelAdmin):
    list_display = ('city_name', 'updated_at', 'condition',
                    'temperature', 'temperature_min', 'temperature_max',
                    'wind_speed', )


admin.site.register(CityWeather, CityWeatherAdmin)
